#!/bin/bash

VENV_NAME=".hrun"
if [[ ! -z "$1" ]]; then
    VENV_NAME=$1
fi

if [[ -d ${VENV_NAME} ]]; then
    echo -e "WARNING! Environment with name '${VENV_NAME}' is exists! >>> removed"
    rm -rf ${VENV_NAME}
fi

echo "Creating Python virtual environment \"${VENV_NAME}\""
python -m venv ${VENV_NAME}

source ${VENV_NAME}/bin/activate

echo "Install requirement packages"
pip -q install -r requirements.txt 

echo "Copy plugins"
rm -rf ~/.sopel/plugins
cp -r plugins ~/.sopel/

echo "Preparing done."

