#!/bin/bash

find_source_files() {
    find . -name '*.py' -size +0 -print | grep -ve './configs' -e './init' -e './.hrun' -e './.venv'
}

files=$(find_source_files)
exit_code=0

fail_py3_unicode=false
for file in ${files}; do
    if grep -qle 'unicode(' -e 'class .*(unicode)' $file; then
        if ! grep -ql 'unicode = str' $file; then
            echo "Suspicious 'unicode' use: $file"
            fail_py3_unicode=true
        fi
    fi
done
if $fail_py3_unicode; then
    echo "ERROR: Above files use unicode() but do not make it safe for Python 3."
    exit_code=1
fi

flake8 --config .flake8 ${files}

if [ $? -ne 0 ]; then
    exit_code=1
fi
exit $exit_code
