#!/usr/bin/env python
from sys import exit as sys_exit
from glob import glob
from os.path import basename, splitext, exists as file_exists
from configparser import RawConfigParser
from argparse import ArgumentParser


# Argument parser
arg_parser = ArgumentParser(description='Util to find and set all plugins in config')
arg_parser.add_argument(
    '-c',
    action='store',
    dest='config_filename',
    required=True,
    help='Set path to config.')
arg_parser.add_argument(
    '-p',
    action='store',
    dest='plugins',
    required=True,
    help='Set path to plugins')
console_params = arg_parser.parse_args()

# Check existing of config filename and directory for plugins
if not file_exists(console_params.config_filename):
    print(f"ERROR. Config {console_params.config_filename} does not exists")
    sys_exit(1)
config_filename = console_params.config_filename
if not file_exists(console_params.plugins):
    print(f"ERROR. Directory for plugins {console_params.plugins} does not exists")
    sys_exit(1)
plugins = console_params.plugins + '/*.py'

# Getting plugin names from plugin dirs
raw_files = glob(plugins)
files = list()
for raw_file in raw_files:
    ffile = splitext(basename(raw_file))[0]
    files.append(ffile)

value_files = '\n' + '\n'.join(item for item in files)

# Read config files
parser = RawConfigParser(allow_no_value=True)
parser.read(config_filename)

# Check option. Exit if doesn't.
if not parser.has_option('core', 'enable'):
    print('ERROR. Config has no option \'enable\'')
    sys_exit(1)

# Write config with plugins finded
parser.set('core', 'enable', value_files)
with open(config_filename, 'w') as config_file:
    parser.write(config_file)
    print('done.')
