.PHONY: quality test coverage_report

quality:
	./scripts/checkstyle.sh

test:
	coverage run -m py.test -v .

coverage_report:
	coverage report
