# coding=utf-8
"""
translate_ru.py - Hrundel Translation Module
Copyright © 2020, rasdark, <admin@rasdark.ru>

Licensed under the Eiffel Forum License 2.
"""
from functools import wraps

from flask import Blueprint
from flask import current_app as app
from flask import jsonify, redirect, request

error_codes = {
    'unauthorized': 'Authorization token required'
}

blueprint_api = Blueprint("api", __name__)


def getKeysByValue(dictOfElements, valueToFind):
    listOfItems = dictOfElements.items()
    for item in listOfItems:
        if item[1] == valueToFind:
            return item[0]
    return None


class InvalidUsage(Exception):
    status_code = 400

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['error_message'] = self.message
        return rv


def require_api_token(func):
    @wraps(func)
    def check_token(*args, **kwargs):
        if request.method != 'POST':
            raise InvalidUsage(
                error_codes['unauthorized'],
                status_code=401,
                payload={"error_code": "UNAUTHORIZED"})
        auth = request.headers.get('Authorization')
        if not auth:
            raise InvalidUsage(
                error_codes['unauthorized'],
                status_code=401,
                payload={"error_code": "UNAUTHORIZED"})
        user = getKeysByValue(app.config['tokens'], auth)
        if not user:
            raise InvalidUsage(
                error_codes['unauthorized'],
                status_code=401,
                payload={"error_code": "UNAUTHORIZED"})
        return func(*args, **kwargs)
    return check_token


@blueprint_api.errorhandler(InvalidUsage)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


@blueprint_api.route('/', methods=['GET', 'POST'])
def api_index():
    return redirect("v1", code=302)


@blueprint_api.route("/v1", methods=["GET", "POST"])
@require_api_token
def api_main():
    # raise InvalidUsage('Invalid usage of that route', status_code=410)
    return {'status': 'ok', 'message': 'access granted'}, 200
