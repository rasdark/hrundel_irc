# coding=utf-8
"""
reload_ru.py - Hrundel Module Reloader Module
Copyright © 2020, rasdark, <admin@rasdark.ru>

Licensed under the Eiffel Forum License 2.
"""
from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

import logging
from sys import path

import sopel.module
from sopel import config, plugins

path.append(config.DEFAULT_HOMEDIR + '/plugins')

from authdb.decorators import (require_authuser, require_chanadmin,  # noqa
                               require_founder, require_globaladmin)

LOGGER = logging.getLogger(f'sopel.{__name__}')


def _load(bot, plugin):
    # handle errors while loading (if any)
    try:
        plugin.load()
        if plugin.has_setup():
            plugin.setup(bot)
        plugin.register(bot)
    except Exception as e:
        LOGGER.exception('Error loading %s: %s', plugin.name, e)
        raise


@sopel.module.nickname_commands("reload")
@require_authuser
@require_founder
@sopel.module.priority("low")
@sopel.module.thread(False)
def f_reload(bot, trigger):
    """Reloads a module (for use by founder only)."""
    name = trigger.group(2)

    if (not name or name == '*' or
            name.upper() == 'ALL THE THINGS' or
            name.upper() == 'ВСЕ'):
        bot.reload_plugins()
        return bot.say('Перезагрузка модулей выполнена.', trigger.nick)

    if not bot.has_plugin(name):
        return bot.say(f'"{name}" не загружен, попробуй сперва команду `load`')

    bot.reload_plugin(name)
    plugin_meta = bot.get_plugin_meta(name)
    plugin_path = plugin_meta['source'].replace(sopel.config.DEFAULT_HOMEDIR, "<user>")
    return bot.say(f'Выполнено: модуль "{name}" перезагружен ({plugin_path})')


@sopel.module.nickname_commands("load")
@require_authuser
@require_founder
@sopel.module.priority("low")
@sopel.module.thread(False)
def f_load(bot, trigger):
    """Loads a module (for use by founder only)."""
    name = trigger.group(2)
    if not name:
        return

    if bot.has_plugin(name):
        return bot.say('Этот плагин уже был загружен. Попробуй `reload`')

    usable_plugins = plugins.get_usable_plugins(bot.config)
    if name not in usable_plugins:
        return bot.say(f'Плагин "{name}" не найден')

    plugin, is_enabled = usable_plugins[name]
    if not is_enabled:
        return bot.say(f'Этот плагин "{name}" отключен.')

    try:
        _load(bot, plugin)
    except Exception as error:
        bot.say(f'Не могу загрузить плагин "{name}": {error}')
    else:
        plugin_meta = bot.get_plugin_meta(name)
        plugin_path = plugin_meta['source'].replace(sopel.config.DEFAULT_HOMEDIR, "<user>")
        bot.say(f'Плагин "{name}" загружен ({plugin_path})')


# @sopel.module.nickname_commands('update')
# @sopel.module.require_admin
# def f_update(bot, trigger):
#    """Pulls the latest versions of all modules from Git (for use by admins only)."""
#    proc = subprocess.Popen('/usr/bin/git pull',
#                            stdout=subprocess.PIPE,
#                            stderr=subprocess.PIPE, shell=True)
#    bot.reply(proc.communicate()[0])

#    f_reload(bot, trigger)


# Catch private messages
@sopel.module.commands("reload")
@sopel.module.priority("low")
@sopel.module.thread(False)
def pm_f_reload(bot, trigger):
    """Выгружает и снова загружает модуль. Или все модули, если запущен без параметров."""
    if trigger.is_privmsg:
        f_reload(bot, trigger)


@sopel.module.commands("load")
@sopel.module.priority("low")
@sopel.module.thread(False)
def pm_f_load(bot, trigger):
    """Загружает указанный модуль."""
    if trigger.is_privmsg:
        f_load(bot, trigger)


# @sopel.module.commands('update')
# def pm_f_update(bot, trigger):
#    """Обновляет модули с сайта разработчика."""
#    if trigger.is_privmsg:
#        f_update(bot, trigger)
