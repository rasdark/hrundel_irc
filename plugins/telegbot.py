# coding=utf-8
"""
telegbot.py - Telegram-bot started by Hrundel
Copyright © 2021, rasdark, <admin@rasdark.ru>

Licensed under the Eiffel Forum License 2.
"""
from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

import base64
import logging
import os.path
import re
import threading
import uuid
from os import remove as _remove_file
from sys import path
from time import sleep

import requests
import speech_recognition as sr
import telebot
from sopel import __version__ as irc_release
from sopel import config
from sopel.config.types import ListAttribute, StaticSection, ValidatedAttribute
from sopel.module import require_chanmsg, rule
from telebot.version import __version__ as telebot_version

path.append(config.DEFAULT_HOMEDIR + '/plugins')

from translate_ru import translate  # noqa

LOGGER = logging.getLogger(f'sopel.{__name__}')

_version_ = '0.1.5-dev080'

MAX_FILE_SIZE = 50 * 1024 * 1024 * 1024
MAX_CAPTION_SIZE = 150
MAX_TEXT_SIZE = 250

langs_map = {
    'английский': 'en',
    'испанский': 'es',
    'немецкий': 'de',
    'французский': 'fr',
    'итальянский': 'it',
    'китайский': 'zh'
}

commands_map = {
    'version': 'Показывает версию телеграмм-бота',
    'help': 'Показывает информацию о всех доступных командах телеграмм-бота',
    'list': 'Показывает список IRC пользователей канала',
    'ping': 'Проверяет на связи ли телеграмм-бот'
}


class PostingException(Exception):
    """Custom exception type for errors posting help to the chosen pastebin."""
    pass


def _requests_post_catch_errors(*args, **kwargs):
    try:
        response = requests.post(*args, **kwargs)
        response.raise_for_status()
    except (
            requests.exceptions.Timeout,
            requests.exceptions.TooManyRedirects,
            requests.exceptions.RequestException,
            requests.exceptions.HTTPError
    ):
        LOGGER.exception('Error during POST request')
        raise PostingException('Could not communicate with remote service')
    return response


def post_to_rasdark(msg):
    try:
        result = _requests_post_catch_errors(
            'https://rasdark.ru/paste/+upload',
            data={'text': msg,
                  'maxlife-unit': 'WEEKS',
                  'maxlife-value': 2})

    except PostingException:
        raise

    if result.status_code == 200:
        return result.url.replace('#None', '', 1)
    else:
        LOGGER.error("Invalid result %s", result)
        raise PostingException('"rasdark" result did not contain '
                               'expected URL base.')


def getKeysByValue(dictOfElements, valueToFind):
    listOfItems = dictOfElements.items()
    for item in listOfItems:
        if item[1] == valueToFind:
            return item[0]
    return None


def escape_markdown(text: str, version: int = 1, entity_type: str = None) -> str:
    if int(version) == 1:
        escape_chars = r'_*`['
    elif int(version) == 2:
        if entity_type in ['pre', 'code']:
            escape_chars = r'\`'
        elif entity_type == 'text_link':
            escape_chars = r'\)'
        else:
            escape_chars = r'_*[]()~`>#+-=|{}.!'
    else:
        raise ValueError('Markdown version must be either 1 or 2!')

    return re.sub(f'([{re.escape(escape_chars)}])', r'\\\1', text)


# Telegram-IRC bot gateway

class TelegramSection(StaticSection):
    token = ValidatedAttribute('token')
    chan_map = ListAttribute('chan_map')
    images = ValidatedAttribute('images', bool, default=False)
    image_directory = ValidatedAttribute('image_directory')
    url_prefix = ValidatedAttribute('url_prefix')


def configure(config):
    config.define_section('telegram', TelegramSection)


def telebot_actions(bot_irc):
    def is_admin(chat_id, user_id):
        admins = bot_telegram.get_chat_administrators(chat_id)
        for admin in admins:
            if user_id == admin.user.id:
                return True
        return False

    # command test
    @bot_telegram.message_handler(commands=['test', 'тест'])
    def on_command_test(m):
        if m.chat.type not in ['group', 'supergroup', 'channel']:
            return
        cid = m.chat.id
        if is_admin(cid, m.from_user.id):
            bot_telegram.reply_to(m, 'Обнаружен админ Телеграм чата!')
        else:
            bot_telegram.reply_to(m, 'Не повезло... Приходите в следующий раз!')

    # command help
    @bot_telegram.message_handler(commands=['help', 'помощь'])
    def on_command_help(m):
        LOGGER.info(f'@{m.from_user.username} 🠖 help')
        help_text = "Доступны следующие команды: \n"
        for key in commands_map:
            help_text += "/" + key + ": "
            help_text += commands_map[key] + "\n"
        bot_telegram.reply_to(m, help_text)

    # command version
    @bot_telegram.message_handler(commands=['version', 'версия'])
    def on_command_version(m):
        LOGGER.info(f'@{m.from_user.username} 🠖 version')
        bot_telegram.reply_to(
            m,
            f"*Hrundel* IRC: {irc_release}\n"
            f"*Хрюндель* Telegram: {_version_}\n\n"
            f"*pyTelegramBotAPI*: {telebot_version}\n"
            f"*SpeechRecognition*: {sr.__version__}",
            parse_mode='markdown')

    # command version
    @bot_telegram.message_handler(commands=['ping', 'пинг'])
    def on_command_ping(m):
        LOGGER.info(f'@{m.from_user.username} 🠖 ping')
        bot_telegram.send_chat_action(m.chat.id, 'typing')
        sleep(3)
        bot_telegram.reply_to(m, "Привет :) Я жив!")

    # command version
    @bot_telegram.message_handler(commands=['list', 'список'])
    def on_command_list(m):
        chat_telegram = m.chat.id
        chan_irc = getKeysByValue(channels, chat_telegram)
        if not chan_irc:
            return
        LOGGER.info(f'@{m.from_user.username} 🠖 list')
        # if m.from_user.username in ["rasdark", "andrex"]:
        if m.chat.type in ['group', 'supergroup', 'channel']:
            line = ''
            for user in bot_irc.channels[chan_irc].users:
                host = bot_irc.users[user].host
                # LOGGER.info(f'{user}@{host}')
                line += f'✔  <b>{user}</b>   ({host})\n'
            # LOGGER.info(f'IRC users:\n{line}')
            bot_telegram.send_chat_action(m.chat.id, 'typing')
            sleep(3)
            bot_telegram.reply_to(
                m,
                f"Список <b>IRC</b> пользователей на канале <b>{chan_irc}</b>:\n\n{line}",
                parse_mode='html')

    # handler for catch photo, video, documents
    @bot_telegram.message_handler(func=lambda message: True, content_types=['photo', 'video', 'document'])
    def on_post_photo(m):
        if bot_irc.config.telegram.images:

            if m.content_type == 'photo':
                message_inf = 'картинка'
                message_ro = 'картинке'
                t_file_id = m.photo[-1].file_id
            if m.content_type == 'video':
                message_inf = 'видосик'
                message_ro = 'видосику'
                t_file_id = m.video.file_id
            if m.content_type == 'document':
                message_inf = 'документик'
                message_ro = 'документику'
                t_file_id = m.document.file_id

            chat_telegram = m.chat.id
            chan_irc = getKeysByValue(channels, chat_telegram)
            if not chan_irc:
                return

            if m.from_user.username:
                from_user = m.from_user.username
            else:
                from_user = m.from_user.first_name
                if m.from_user.last_name:
                    from_user += f' {m.from_user.last_name}'

            t_file_info = bot_telegram.get_file(t_file_id)

            # LOGGER.info(f'file_path = {t_file_info}')

            file_extension = os.path.splitext(t_file_info.file_path)[1]
            if m.content_type == 'document':
                file_name = m.document.file_name
                LOGGER.info(f'{file_name}')
            else:
                random_uuid = base64.urlsafe_b64encode(uuid.uuid4().bytes)
                file_name = '%s%s' % (random_uuid.decode('utf-8').replace('=', ''), file_extension)
            file_path = '%s%s' % (bot_irc.config.telegram.image_directory, file_name)
            file_url = '%s%s' % (bot_irc.config.telegram.url_prefix, file_name)
            if t_file_info.file_size >= MAX_FILE_SIZE:
                LOGGER.info('запостили слишком толстый файл')
                return
            downloaded_file = bot_telegram.download_file(t_file_info.file_path)
            with open(file_path, 'wb') as new_file:
                new_file.write(downloaded_file)

            image_message = f'<{from_user}> {message_inf}: <{file_url}>'
            caption_message = None
            if m.caption:
                caption = m.caption
                if len(caption) > MAX_CAPTION_SIZE:
                    caption_url = post_to_rasdark(caption)
                    if caption_url:
                        caption_message = f'<{from_user}> Подпись к {message_ro} читаем тут: {caption_url}'
                    else:
                        caption_message = f'<{from_user}> по каким-то причинам, подпись не попала на пасту :('
                else:
                    image_message = f'<{from_user}> {message_inf} \'{caption}\' : {file_url}'

            if image_message:
                bot_irc.say(image_message, chan_irc, max_messages=1)
            if caption_message:
                bot_irc.say(caption_message, chan_irc, max_messages=1)

    # handler for catch sticker
    @bot_telegram.message_handler(func=lambda message: True, content_types=['sticker'])
    def on_post_sticker(m):
        chat_telegram = m.chat.id
        chan_irc = getKeysByValue(channels, chat_telegram)
        if not chan_irc:
            return

        if m.from_user.username:
            from_user = m.from_user.username
        else:
            from_user = m.from_user.first_name
            if m.from_user.last_name:
                from_user += f' {m.from_user.last_name}'

        message = f'<{from_user}> {m.sticker.emoji}  (СтикерПак: {m.sticker.set_name})'
        bot_irc.say(message, chan_irc, max_messages=1)

    # handler for catch join new member on telegram channel
    @bot_telegram.message_handler(func=lambda message: True, content_types=["new_chat_members"])
    def on_join_member(m):
        chat_telegram = m.chat.id
        chan_irc = getKeysByValue(channels, chat_telegram)
        if not chan_irc:
            return

        for user in m.new_chat_members:
            user_name = 'без_логина'
            if user.username:
                user_name = user.username
            user_full_name = user.first_name
            if user.last_name:
                user_full_name += f' {user.last_name}'
            is_bot = user.is_bot
            if is_bot:
                message = f'Внимание! К нам пришёл бот {user_full_name}> с логином <{user_name}>'
                message_t = f'Внимание! К нам пришёл бот <{escape_markdown(user_full_name)}> с логином <{escape_markdown(user_name)}>'
                bot_irc.say(message, chan_irc, max_messages=1)
                bot_telegram.send_message(chat_telegram, message_t)
            else:
                message = f'К нам присоединился новый пользователь <{user_full_name}> с логином <{user_name}>'
                message_t = f'К нам присоединился новый пользователь <{escape_markdown(user_full_name)}> с логином <{escape_markdown(user_name)}>'
                welcome = f"*@{user_name}*, добро пожаловать на наш канал :-)"
                if not user.username:
                    message = f'К нам присоединился новый пользователь <{user_full_name}>'
                    message_t = f'К нам присоединился новый пользователь <{escape_markdown(user_full_name)}>'
                    welcome = f"*{user_full_name}*, добро пожаловать на наш канал :-)"
                bot_irc.say(message, chan_irc, max_messages=1)
                bot_telegram.send_message(chat_telegram, f"*Внимание!* {message_t}", parse_mode='Markdown')
                bot_telegram.send_message(chat_telegram, welcome, parse_mode='Markdown')

    # handler for catch part member on telegram channel
    @bot_telegram.message_handler(func=lambda message: True, content_types=["left_chat_member"])
    def on_part_member(m):
        chat_telegram = m.chat.id
        chan_irc = getKeysByValue(channels, chat_telegram)
        if not chan_irc:
            return
        user_name = 'без_логина'
        if m.left_chat_member.username:
            user_name = m.left_chat_member.username
        user_full_name = m.left_chat_member.first_name
        if m.left_chat_member.last_name:
            user_full_name += f' {m.left_chat_member.last_name}'
        message = f'Нашу тёплую компашку покинул пользователь <{user_full_name}> с логином <{user_name}>'
        bot_irc.say(message, chan_irc, max_messages=1)
        bot_telegram.send_message(chat_telegram, message)

    @bot_telegram.message_handler(func=lambda message: True, content_types=['voice'])
    def on_voice_message(m):
        chat_telegram = m.chat.id
        chan_irc = getKeysByValue(channels, chat_telegram)
        if not chan_irc:
            return

        lang = 'ru'
        if m.voice.title:
            lang = m.voice.title

        if m.from_user.username:
            from_user = m.from_user.username
        else:
            from_user = m.from_user.first_name
            if m.from_user.last_name:
                from_user += f' {m.from_user.last_name}'

        t_file_id = m.voice.file_id
        t_file_info = bot_telegram.get_file(t_file_id)
        file_extension = os.path.splitext(t_file_info.file_path)[1]
        random_uuid = base64.urlsafe_b64encode(uuid.uuid4().bytes)
        file_name_random = random_uuid.decode('utf-8').replace('=', '')
        file_name = f'{file_name_random}{file_extension}'
        file_path_config = bot_irc.config.telegram.image_directory
        file_path = f'{file_path_config}{file_name}'
        LOGGER.info(f'Сохраняю звуковой файл... {file_name}')

        downloaded_file = bot_telegram.download_file(t_file_info.file_path)
        with open(file_path, 'wb') as new_file:
            new_file.write(downloaded_file)

        # convert to wav
        import subprocess
        dest_filename = f'{file_path_config}{file_name_random}.wav'

        process = subprocess.run(['ffmpeg', '-i', file_path, dest_filename],
                                 stderr=subprocess.DEVNULL,
                                 stdout=subprocess.DEVNULL)
        if process.returncode != 0:
            raise Exception("Something went wrong")

        # try to recognition
        r = sr.Recognizer()
        with sr.AudioFile(dest_filename) as source:
            audio = r.record(source)
        try:
            text = r.recognize_google(audio, language=lang)
            LOGGER.info(f"[speech] [google]: {text}")
            message = f'[ голос ] {text}'
            words = text.split()
            if words[0].lower() == 'переведи':
                text_to_translate = ' '.join(words[3:])
                to_lang = words[2]
                if to_lang.lower() in langs_map:
                    lang = langs_map[to_lang.lower()]
                    (translated, l) = translate(text_to_translate, 'ru', lang)
                    message_translated = f'[ перевод ] {translated}'
                    bot_irc.say(f'<{from_user}> [ голос ]{message_translated}', chan_irc)
                else:
                    message_translated = f'Извините, я пока не умею переводить на {to_lang.lower()} язык'
                LOGGER.info(message_translated)
                bot_telegram.reply_to(m, message_translated)
            else:
                bot_telegram.reply_to(m, message)
                bot_irc.say(f'<{from_user}> ' + message, chan_irc)
            # LOGGER.info(f'{words[:3]}')
        except sr.UnknownValueError:
            LOGGER.error("Google не понимает что ты сказал")
            bot_telegram.reply_to(m, 'Google не понял что ты сказал...:(')
        except sr.RequestError as e:
            LOGGER.error(f"Google не может получить/отдать результат; {e}")

        LOGGER.info('удаляем временные звуковые файлы...')
        _remove_file(file_path)
        _remove_file(dest_filename)

    # default handler for every other text
    @bot_telegram.message_handler(func=lambda message: True, content_types=['text'])
    def on_post_messsage(m):
        chat_telegram = m.chat.id
        chan_irc = getKeysByValue(channels, chat_telegram)
        if not chan_irc:
            return

        if m.from_user.username:
            from_user = m.from_user.username
        else:
            from_user = m.from_user.first_name
            if m.from_user.last_name:
                from_user += f' {m.from_user.last_name}'

        text = m.text
        if len(text) > MAX_TEXT_SIZE:
            url = post_to_rasdark(text)
            if url:
                message = f'<{from_user}> длинный пост: {url}'
            else:
                message = f'<{from_user}> мой длинный пост, по каким-то причинам, не попал на пасту :('
        else:
            message = f'<{from_user}> {text}'

        bot_irc.say(message, chan_irc, max_messages=1)

    # default handler for every message

    # Обработка данных
    @bot_telegram.callback_query_handler(func=lambda call: True)
    def handler_call(call):
        LOGGER.info(f'{call}')


def telebot_polling(bot_irc):
    global bot_telegram
    while True:
        try:
            LOGGER.info("Запущен новый экземпляр Telegram бота.")
            bot_telegram = telebot.TeleBot(bot_irc.config.telegram.token)
            telebot_actions(bot_irc)
            # bot_telegram.set_update_listener(telebot_listener)
            bot_telegram.polling(
                none_stop=True,
                interval=3,
                timeout=30,
                long_polling_timeout=30)
        except Exception:
            import traceback
            LOGGER.info(f"Произошла какая-то ошибка Telegram бота.\n"
                        f"{traceback.format_exc()}")
            bot_telegram.stop_polling()
            sleep(3)
        else:
            bot_telegram.stop_polling()
            LOGGER.info("Telegram-бот остановлен.")
            break


def setup(bot_irc):
    global channels

    channels = {}

    bot_irc.config.define_section('telegram', TelegramSection)

    chans = bot_irc.config.telegram.chan_map
    for line in chans:
        (chan, chat_id) = line.split()
        for raw_chan in bot_irc.config.core.channels:
            if chan == raw_chan.split()[0]:
                channels[chan] = int(chat_id)
                break

    polling_thread = threading.Thread(
        target=telebot_polling,
        daemon=True,
        args=(bot_irc,))
    polling_thread.start()


@require_chanmsg
@rule('.+')
def send_to_telegram(bot, trigger):
    global bot_telegram
    global channels
    if trigger.sender in channels.keys():
        message = escape_markdown(trigger)
        bot_telegram.send_message(
            int(channels[trigger.sender]),
            f"*{trigger.nick} (IRC)*: {message}",
            parse_mode='markdown')
