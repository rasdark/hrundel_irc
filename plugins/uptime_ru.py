# coding=utf-8
"""
uptime_ru.py - Hrundel Uptime Module
Copyright © 2020, rasdark, <admin@rasdark.ru>

Licensed under the Eiffel Forum License 2.
"""
from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

import logging
from datetime import datetime
from sys import path

from psutil import disk_partitions, disk_usage
from sopel import config
from sopel.module import commands

path.append(config.DEFAULT_HOMEDIR + '/plugins')

from authdb.decorators import require_authuser  # noqa

LOGGER = logging.getLogger(f'sopel.{__name__}')

CYRILLIC_MAX = 230


def get_size(bytes, suffix="B"):
    """
        1253656 => '1.20MB'
        1253656678 => '1.17GB'
    """
    factor = 1024
    for unit in ["", "K", "M", "G", "T", "P"]:
        if bytes < factor:
            return f"{bytes:.2f}{unit}{suffix}"
        bytes /= factor


def setup(bot):
    if "start_time" not in bot.memory:
        bot.memory["start_time"] = datetime.utcnow()


def _date_plural(number, date):
    TO_DATE = {
        'год': ['года', 'лет'],
        'месяц': ['месяца', 'месяцев'],
        'день': ['дня', 'дней'],
        'час': ['часа', 'часов'],
        'минута': ['минуты', 'минут'],
        'секунда': ['секунды', 'секунд']
    }
    number_1 = number - number // 10 * 10
    number_10 = number // 10 % 10
    if number_1 == 1 and number_10 != 1:
        return("{} {}".format(number, date))
    elif number_1 > 1 and number_1 < 5 and number_10 != 1:
        return("{} {}".format(number, TO_DATE[date][0]))
    else:
        return("{} {}".format(number, TO_DATE[date][1]))


def _bytes_to_gb(data, key=""):
    if key == "percent":
        return data
    else:
        return round(data / (1024 ** 3), 2)


def _fill_disk():
    info = disk_partitions()
    data = [
        dict(
            device=v.device,
            mount=v.mountpoint,
            fstype=v.fstype,
            opts=v.opts,
            used={
                key: _bytes_to_gb(val, key)
                for key, val in disk_usage(v.mountpoint)._asdict().items()
            }
        )
        for v in info if 'rw' in v.opts
    ]
    # if v.opts == 'rw,fixed'
    return data


def _format_datetime(delta, short=False):
    delta = datetime.utcfromtimestamp(delta)
    string = ""
    year = delta.year - 1970
    if year > 0:
        if short:
            string += f'{year}y '
        else:
            string += _date_plural(year, 'год') + ', '
    month = delta.month - 1
    if month > 0:
        if short:
            string += f'{month}m '
        else:
            string += _date_plural(month, 'месяц') + ', '
    day = delta.day - 1
    if day > 0:
        if short:
            string += f'{day}d '
        else:
            string += _date_plural(day, 'день') + ', '
    if short:
        string += f'{delta.hour:02d}:{delta.minute:02d}:{delta.second:02d}'
    else:
        hour = delta.hour
        if hour > 0:
            string += _date_plural(hour, 'час') + ', '
        minute = delta.minute
        if minute > 0:
            string += _date_plural(minute, 'минута') + ', '
        second = delta.second
        string += _date_plural(second, 'секунда')

    return string


@require_authuser
@commands('uptime', 'аптайм')
def uptime(bot, trigger):
    """.uptime - Returns the uptime of bot."""
    output = _format_datetime(
        (datetime.utcnow() - bot.memory["start_time"]).total_seconds())
    bot.say(trigger.nick + ", я нахожусь тут уже {}!".format(output))


@require_authuser
@commands('sysinfo', 'systeminfo', 'сисинфо')
def sysinfo(bot, trigger):
    """.sysinfo - Показывает краткое инфо о системе, где запущен бот"""
    try:
        import platform
    except Exception as e:
        LOGGER.error('Error of import "platform" module. Try install it first')
        LOGGER.error(str(e))
        return bot.notice('К сожалению, при попытке импорта необходимого'
                          ' модуля произошла ошибка.', trigger.nick)

    uname = platform.uname()
    if uname.system.lower() != 'linux':
        return bot.notice(
            f'К сожалению, получение системной информации на'
            f' платформе "{uname.system}" временно невозможна.',
            trigger.nick)

    output = ''
    output += 'Host: {}'.format(uname.node)

    try:
        import distro
        output += ' | OS: {}'.format(distro.distro_release_info()['name'])
    except Exception as e:
        LOGGER.error('Error of import "distro" module. Try install it first')
        LOGGER.error(str(e))
        output += ' | OS: {} Unknown'.format(uname.system)

    output += ' {}'.format(uname.machine)

    output += ' | {}'.format(uname.release)

    try:
        import psutil
        svmem = psutil.virtual_memory()
        swap = psutil.swap_memory()
        output += ' | Mem: {}/{}%'.format(get_size(svmem.total), svmem.percent)
        if swap.percent > 0:
            output += ' | Swap: {}/{}%'.format(
                get_size(swap.total),
                swap.percent)
        cores = psutil.cpu_count(logical=True)
        freq = psutil.cpu_freq().current
        usage = psutil.cpu_percent()
        output += f' | CPU: {cores} Cores {freq:.2f}MHz {usage}%'

        disk = _fill_disk()
        output += ' | Disks:'
        for x in disk:
            if x['mount'] == '/':
                output += (f" root|{x['fstype']}|{x['used']['total']}ГБ|"
                           f"{x['used']['percent']}%")
    except Exception as e:
        LOGGER.error('Error with usage "psutil" module')
        LOGGER.error('Error: ' + str(e))
        import traceback
        LOGGER.error(traceback.format_exc())

    uptime = None
    with open("/proc/uptime", "r") as f:
        uptime = f.read().split(" ")[0].strip()
    uptime = int(float(uptime))

    uptime_str = _format_datetime(uptime, True)

    output += f' | Uptime: {uptime_str}'

    bot.say(output, trigger.sender)
