# coding=utf-8
"""
translate_ru.py - Hrundel Translation Module
Copyright © 2020, rasdark, <admin@rasdark.ru>

Licensed under the Eiffel Forum License 2.
"""
from logging import getLogger
from sys import path as sys_path
# import secrets
from threading import Thread

from flask import Flask
from sopel import config
from sopel.config.types import (FilenameAttribute, StaticSection,
                                ValidatedAttribute)

sys_path.append(config.DEFAULT_HOMEDIR + '/plugins')

from botapi.views import blueprint_api as api  # noqa

__version__ = '0.0.1-dev01'
logger_bot = getLogger(f'sopel.{__name__}')

tokens = {
    'rasdark': '6GGJsm2pCHS4y2zD28txEKZTAo5zL6jx4X0MY4_lYLU',
    'user': 'valid_token'
}


api_app = Flask(__name__)
api_app.register_blueprint(api)


def api_threaded(bot_irc):
    host = bot_irc.config.bot_api.host
    port = bot_irc.config.bot_api.port
    debug = bot_irc.config.bot_api.debug
    ssl_key = bot_irc.config.bot_api.ssl_key
    ssl_crt = bot_irc.config.bot_api.ssl_crt
    logger_bot.info("Запускаем API сервер...")

    api_app.config['bot_irc'] = bot_irc
    api_app.config['tokens'] = tokens

    if ssl_key and ssl_crt:
        api_app.run(
            host=host,
            port=port,
            debug=debug,
            use_reloader=False,
            ssl_context=(ssl_crt, ssl_key))
    else:
        api_app.run(
            host=host,
            port=port,
            debug=debug,
            use_reloader=False)


class BotApiSection(StaticSection):
    host = ValidatedAttribute('host', str, default='localhost')
    port = ValidatedAttribute('port', int, default=8000)
    debug = ValidatedAttribute('debug', bool, default=False)
    ssl_key = FilenameAttribute('ssl_key', directory=False, default=None)
    ssl_crt = FilenameAttribute('ssl_crt', directory=False, default=None)


def configure(config):
    config.define_section('bot_api', BotApiSection)


def setup(bot_irc):
    bot_irc.config.define_section('bot_api', BotApiSection)

    polling_thread = Thread(
        target=api_threaded,
        daemon=True,
        args=(bot_irc,))
    polling_thread.start()
