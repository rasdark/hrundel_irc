# coding=utf-8
"""Integration tests for the help_ru plugin."""
import pytest
from sopel import plugins
from sopel.tests import rawlist


TMP_CONFIG = """
[core]
owner = testnick
nick = TestBot
enable = coretasks
"""


@pytest.fixture
def tmpconfig(configfactory):
    return configfactory('conf.ini', TMP_CONFIG)


@pytest.fixture
def mockbot(tmpconfig, botfactory):
    help_plugin = plugins.handlers.PyFilePlugin('./plugins/help_ru.py')
    sopel = botfactory(tmpconfig)
    help_plugin.load()
    help_plugin.setup(sopel)
    help_plugin.register(sopel)
    return sopel


@pytest.fixture
def irc(mockbot, ircfactory):
    return ircfactory(mockbot)


def test_help(irc, userfactory):
    user = userfactory('swine')
    irc.pm(user, '.help')

    assert irc.bot.backend.message_sent == rawlist(
        "PRIVMSG swine :Подожди... я готовлю список команд."
    )
